import { ensureConfig, getConfig } from '@edx/frontend-platform';
import { getAuthenticatedHttpClient } from '@edx/frontend-platform/auth';
import { logError } from '@edx/frontend-platform/logging';

import { generateAndSubmitForm } from '../../data/utils';

ensureConfig(['ECOMMERCE_BASE_URL'], 'PayPal API service');

/**
 * Checkout with cash
 *
 * 1. Send the basket_id and payment_processor to our /api/v2/checkout/
 * 2. Receive a paypal url
 * 3. Generate an submit an empty form to the paypal url
 */

export default  async function cashCheckout(basket,formDataCustom) {
    const { basketId } = basket;
    const formData = {
      clientData: formDataCustom,
      basket_id: basketId,
      payment_processor: 'cash',
    };
    if (basket.discountJwt) {
      formData.discount_jwt = basket.discountJwt;
    }
    const { data } = await getAuthenticatedHttpClient()
      .post(`${getConfig().ECOMMERCE_BASE_URL}/payment/cash/basket`, formData)
      .catch((error) => {
        logError(error, {
          messagePrefix: 'cash Checkout Error',
          paymentMethod: 'cash',
          paymentErrorType: 'Checkout',
          basketId,
        });

        throw error;
      });

    const form = global.document.createElement('form');
  form.method = 'GET';
  form.action = data.payment_page_url;

  const hiddenField = global.document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = 'basket_id';
  hiddenField.value =basketId;

  form.appendChild(hiddenField);

  global.document.body.appendChild(form);
  form.submit();
  }
